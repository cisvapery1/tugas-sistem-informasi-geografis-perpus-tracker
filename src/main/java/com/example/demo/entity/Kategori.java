package com.example.demo.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.example.demo.entity.audit.Auditable;

@Entity
@Table(name = "kategori")
public class Kategori extends Auditable<String> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "type")
	private String type;

	@Column(name = "deskripsi")
	private String deskripsi;

	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH }, mappedBy = "kategori")
	private Set<Perpustakaan> perpustakaans;

	public Kategori() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Kategori(String name, String type, String deskripsi) {
		super();
		this.name = name;
		this.type = type;
		this.deskripsi = deskripsi;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDeskripsi() {
		return deskripsi;
	}

	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}

	public Set<Perpustakaan> getPerpustakaans() {
		return perpustakaans;
	}

	public void setPerpustakaans(Set<Perpustakaan> perpustakaans) {
		this.perpustakaans = perpustakaans;
	}

	public void addPerpustakaan(Perpustakaan perpustakaan) {
		if(perpustakaans == null) {
			perpustakaans = new HashSet<>();
		}
		
		perpustakaans.add(perpustakaan);
		perpustakaan.setKategori(this);
	}
	@Override
	public String toString() {
		return "Kategori [id=" + id + ", name=" + name + ", type=" + type + ", deskripsi=" + deskripsi + "]";
	}

}
