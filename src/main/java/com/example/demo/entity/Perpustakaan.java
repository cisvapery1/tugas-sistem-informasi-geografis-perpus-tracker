package com.example.demo.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.example.demo.entity.audit.Auditable;

@Entity
@Table(name = "perpustakaan")
public class Perpustakaan extends Auditable<String> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "latitude")
	private double latitude;

	@Column(name = "longitude")
	private double longitude;

	@Column(name = "name")
	private String name;

	@Column(name = "jam_buka")
	@Temporal(TemporalType.TIME)
	private Date jamBuka;

	@Column(name = "jam_tutup")
	@Temporal(TemporalType.TIME)
	private Date jamTutup;

	@Column(name = "telepon")
	private String telepon;

	@Column(name = "alamat")
	private String alamat;

	@Column(name = "deskripsi")
	private String deskripsi;

	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH })
	@JoinColumn(name = "kategori_id")
	private Kategori kategori;

	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH }, mappedBy = "perpustakaan")
	private Set<GambarPerpustakaan> gambarPerpustakaans;

	public Perpustakaan() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Perpustakaan(double latitude, double longitude, String name, Date jamBuka, Date jamTutup, String telepon,
			String alamat, String deskripsi) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
		this.name = name;
		this.jamBuka = jamBuka;
		this.jamTutup = jamTutup;
		this.telepon = telepon;
		this.alamat = alamat;
		this.deskripsi = deskripsi;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getJamBuka() {
		return jamBuka;
	}

	public void setJamBuka(Date jamBuka) {
		this.jamBuka = jamBuka;
	}

	public Date getJamTutup() {
		return jamTutup;
	}

	public void setJamTutup(Date jamTutup) {
		this.jamTutup = jamTutup;
	}

	public String getTelepon() {
		return telepon;
	}

	public void setTelepon(String telepon) {
		this.telepon = telepon;
	}

	public String getDeskripsi() {
		return deskripsi;
	}

	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}

	public Kategori getKategori() {
		return kategori;
	}

	public void setKategori(Kategori kategori) {
		this.kategori = kategori;
	}

	public Set<GambarPerpustakaan> getGambarPerpustakaans() {
		return gambarPerpustakaans;
	}

	public void setGambarPerpustakaans(Set<GambarPerpustakaan> gambarPerpustakaans) {
		this.gambarPerpustakaans = gambarPerpustakaans;
	}

	public void addGambar(GambarPerpustakaan gambar) {
		if (gambarPerpustakaans == null) {
			gambarPerpustakaans = new HashSet<>();
		}

		gambarPerpustakaans.add(gambar);
		gambar.setPerpustakaan(this);
	}

	@Override
	public String toString() {
		return "Perpustakaan [id=" + id + ", latitude=" + latitude + ", longitude=" + longitude + ", name=" + name
				+ ", jamBuka=" + jamBuka + ", jamTutup=" + jamTutup + ", telepon=" + telepon + ", deskripsi="
				+ deskripsi + "]";
	}

}
