package com.example.demo.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.entity.Kategori;
import com.example.demo.form.KategoriForm;
import com.example.demo.service.KategoriService;

@Controller
@RequestMapping("kategori")
public class KategoriController {

	@Autowired
	private KategoriService kategoriService;

	@GetMapping
	public String index() {
		return "kategori/index";
	}

	@GetMapping("/add")
	public String add(Model model, KategoriForm kategoriForm) {
		return "kategori/add";
	}

	@PostMapping("/add")
	public String addAction(Model model, @Valid KategoriForm kategoriForm, BindingResult result) {

		System.out.println(result);
		if (result.hasErrors()) {
			return "kategori/add";
		}

		String name = kategoriForm.getName();
		String type = kategoriForm.getType();
		String deskripsi = kategoriForm.getDeskripsi();

		Kategori kategori = new Kategori();
		kategori.setName(name);
		kategori.setType(type);
		kategori.setDeskripsi(deskripsi);

		kategoriService.save(kategori);
		model.addAttribute("message", "Data berhasil disimpan.");
		return "kategori/index";
	}

	@GetMapping("/edit/{id}")
	public String edit(@PathVariable(value = "id") Long id, Model model, KategoriForm kategoriForm) {

		if (!kategoriService.dtoFindById(id).isPresent()) {
			model.addAttribute("message", "Data tidak ditemukan.");
			return "kategori/add";
		}

		Kategori kategori = kategoriService.findById(id);
		kategoriForm.setId(kategori.getId());
		kategoriForm.setName(kategori.getName());
		kategoriForm.setType(kategori.getType());
		kategoriForm.setDeskripsi(kategori.getDeskripsi());

		return "kategori/edit";
	}

	@PostMapping("/edit/{id}")
	public String editAction(@PathVariable(value = "id") Long id, Model model, @Valid KategoriForm kategoriForm,
			BindingResult result) {

		System.out.println(result);
		if (result.hasErrors()) {
			Kategori kategori = kategoriService.findById(id);
			kategoriForm.setId(kategori.getId());
			kategoriForm.setName(kategori.getName());
			kategoriForm.setType(kategori.getType());
			kategoriForm.setDeskripsi(kategori.getDeskripsi());
			return "kategori/edit";
		}

		String name = kategoriForm.getName();
		String type = kategoriForm.getType();
		String deskripsi = kategoriForm.getDeskripsi();

		Kategori kategori = kategoriService.findById(id);
		kategori.setName(name);
		kategori.setType(type);
		kategori.setDeskripsi(deskripsi);

		kategoriService.save(kategori);
		model.addAttribute("message", "Data berhasil diedit.");
		return "kategori/index";
	}
}