package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.example.demo.repository.PerpustakaanRepository;

@Controller
public class HomeController {

	@Autowired
	private PerpustakaanRepository perpustakaanRepository;
	
	@GetMapping("/")
	public String map(Model model) {
		model.addAttribute("perpustakaans", perpustakaanRepository.findAll());
		System.out.println(perpustakaanRepository.findAll());
		
		return "index";
	}
}