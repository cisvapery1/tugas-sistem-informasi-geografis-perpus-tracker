package com.example.demo.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.entity.Perpustakaan;
import com.example.demo.form.PerpustakaanForm;
import com.example.demo.repository.KategoriRepository;
import com.example.demo.repository.PerpustakaanRepository;
import com.example.demo.service.PerpustakaanService;
import com.example.demo.util.LocalDateUtil;

@Controller
@RequestMapping("perpustakaan")
public class PerpustakaanController {

	@Autowired
	private KategoriRepository kategoriRepository;

	@Autowired
	private PerpustakaanService perpustakaanService;

	@Autowired
	private PerpustakaanRepository perpustakaanRepository;

	@Autowired
	private LocalDateUtil localDateUtil;

	@GetMapping
	public String index() {
		return "perpustakaan/index";
	}

	@GetMapping("add")
	public String add(PerpustakaanForm perpustakaanForm, Model model) {
		model.addAttribute("kategoris", kategoriRepository.findAll());
		return "perpustakaan/add";
	}

	@PostMapping("add")
	public String addAction(@Valid PerpustakaanForm perpustakaanForm, BindingResult result, Model model) {

		System.out.println(result);
		if (result.hasErrors()) {
			model.addAttribute("kategoris", kategoriRepository.findAll());
			return "perpustakaan/add";
		}
		String alamat = perpustakaanForm.getAlamat();
		double latitude = perpustakaanForm.getLatitude();
		double longitude = perpustakaanForm.getLongitude();
		String name = perpustakaanForm.getName();
		String deskripsi = perpustakaanForm.getDeskripsi();
		String jamBuka = perpustakaanForm.getJamBuka();
		String jamTutup = perpustakaanForm.getJamTutup();
		Long kategoriId = perpustakaanForm.getKategoriId();
		String telepon = perpustakaanForm.getTelepon();

		System.out.println(localDateUtil.toDate(jamBuka));
		System.out.println(localDateUtil.toDate(jamTutup));
		System.out.println(perpustakaanForm.toString());

		Perpustakaan perpus = new Perpustakaan();
		perpus.setAlamat(alamat);
		perpus.setDeskripsi(deskripsi);
		perpus.setJamBuka(localDateUtil.toDate(jamBuka));
		perpus.setJamTutup(localDateUtil.toDate(jamTutup));
		perpus.setKategori(kategoriRepository.findById(Long.valueOf(kategoriId)).get());
		perpus.setTelepon(telepon);
		perpus.setLatitude(latitude);
		perpus.setLongitude(longitude);
		perpus.setName(name);
		perpustakaanService.save(perpus);

		return "perpustakaan/index";
	}

	@GetMapping("edit/{id}")
	public String edit(@PathVariable("id") Long id, PerpustakaanForm perpustakaanForm, Model model) {
		if (!perpustakaanRepository.findById(id).isPresent()) {
			model.addAttribute("message", "Data tidak ditemukan.");
			return "perpustakaan/index";
		}
		model.addAttribute("kategoris", kategoriRepository.findAll());
		Perpustakaan perpus = perpustakaanRepository.findById(id).get();
		perpustakaanForm.setAlamat(perpus.getAlamat());
		perpustakaanForm.setDeskripsi(perpus.getDeskripsi());
		perpustakaanForm.setId(perpustakaanForm.getId());
		perpustakaanForm.setJamBuka(localDateUtil.asLocalDateTime(perpus.getJamBuka(), "HH:mm"));
		perpustakaanForm.setJamTutup(localDateUtil.asLocalDateTime(perpus.getJamTutup(), "HH:mm"));
		perpustakaanForm.setKategoriId(perpus.getKategori().getId());
		perpustakaanForm.setLatitude(perpus.getLatitude());
		perpustakaanForm.setLongitude(perpus.getLongitude());
		perpustakaanForm.setName(perpus.getName());
		perpustakaanForm.setTelepon(perpus.getTelepon());
		return "perpustakaan/edit";
	}

	@PostMapping("edit/{id}")
	public String editAction(@PathVariable("id") Long id, @Valid PerpustakaanForm perpustakaanForm,
			BindingResult result, Model model) {

		System.out.println(result);
		if (result.hasErrors()) {
			model.addAttribute("kategoris", kategoriRepository.findAll());
			Perpustakaan perpus = perpustakaanRepository.findById(id).get();
			perpustakaanForm.setAlamat(perpus.getAlamat());
			perpustakaanForm.setDeskripsi(perpus.getDeskripsi());
			perpustakaanForm.setId(perpustakaanForm.getId());
			perpustakaanForm.setJamBuka(localDateUtil.asLocalDateTime(perpus.getJamBuka(), "HH:mm"));
			perpustakaanForm.setJamTutup(localDateUtil.asLocalDateTime(perpus.getJamTutup(), "HH:mm"));
			perpustakaanForm.setKategoriId(perpus.getKategori().getId());
			perpustakaanForm.setLatitude(perpus.getLatitude());
			perpustakaanForm.setLongitude(perpus.getLongitude());
			perpustakaanForm.setName(perpus.getName());
			perpustakaanForm.setTelepon(perpus.getTelepon());
			return "perpustakaan/edit";
		}
		
		String alamat = perpustakaanForm.getAlamat();
		double latitude = perpustakaanForm.getLatitude();
		double longitude = perpustakaanForm.getLongitude();
		String name = perpustakaanForm.getName();
		String deskripsi = perpustakaanForm.getDeskripsi();
		String jamBuka = perpustakaanForm.getJamBuka();
		String jamTutup = perpustakaanForm.getJamTutup();
		Long kategoriId = perpustakaanForm.getKategoriId();
		String telepon = perpustakaanForm.getTelepon();

		System.out.println(localDateUtil.toDate(jamBuka));
		System.out.println(localDateUtil.toDate(jamTutup));
		System.out.println(perpustakaanForm.toString());

		Perpustakaan perpus = perpustakaanRepository.findById(id).get();
		perpus.setAlamat(alamat);
		perpus.setDeskripsi(deskripsi);
		perpus.setJamBuka(localDateUtil.toDate(jamBuka));
		perpus.setJamTutup(localDateUtil.toDate(jamTutup));
		perpus.setKategori(kategoriRepository.findById(kategoriId).get());
		perpus.setTelepon(telepon);
		perpus.setLatitude(latitude);
		perpus.setLongitude(longitude);
		perpus.setName(name);
		perpustakaanService.save(perpus);

		return "perpustakaan/index";
	}
}
