package com.example.demo.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.PerpustakaanDto;
import com.example.demo.repository.PerpustakaanRepository;

@RestController
@RequestMapping("/api")
public class PerpustakanRestController {

	@Autowired
	private PerpustakaanRepository perpustakaanRepository;
	
	@GetMapping("/perpustakaan/{id}")
	public PerpustakaanDto findPerpustakaanById(@PathVariable("id") Long id) {
		return perpustakaanRepository.dtoFindById(id).get();
	}
}
