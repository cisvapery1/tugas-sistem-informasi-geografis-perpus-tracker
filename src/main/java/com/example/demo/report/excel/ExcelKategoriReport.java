package com.example.demo.report.excel;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import com.example.demo.dto.KategoriDto;


public class ExcelKategoriReport extends AbstractXlsView{

	public ExcelKategoriReport() {
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		response.setHeader("Content-Disposition", "attachment; filename=\"Report.xls\"");

		@SuppressWarnings("unchecked")
		List<KategoriDto> lists = (List<KategoriDto>) model.get("reports");
		Integer number = (Integer) model.get("number");
		Integer size = (Integer) model.get("size");
		
		Sheet s = workbook.createSheet("Report");

		Row row = s.createRow(0);
		row.createCell(0).setCellValue("No");
		row.createCell(1).setCellValue("Nama");
		row.createCell(2).setCellValue("Type");
		row.createCell(3).setCellValue("Deskripsi");

		// Create a Font for styling header cells
		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 12);
		headerFont.setColor(IndexedColors.WHITE.getIndex());

		// Create a CellStyle with the font
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
		headerCellStyle.setFillBackgroundColor(IndexedColors.BLUE.getIndex());
		headerCellStyle.setAlignment(HorizontalAlignment.CENTER);
		headerCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		headerCellStyle.setFillForegroundColor(IndexedColors.BLUE.getIndex());
		headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		row.getCell(0).setCellStyle(headerCellStyle);
		row.getCell(1).setCellStyle(headerCellStyle);
		row.getCell(2).setCellStyle(headerCellStyle);
		row.getCell(3).setCellStyle(headerCellStyle);
		
		int rowNum = 1;
		int i = 0;
		for (KategoriDto o : lists) {
			Row r = s.createRow(rowNum++);
			r.createCell(0).setCellValue(String.valueOf((number*size) + (i+1)));
			r.createCell(1).setCellValue(o.getName());
			r.createCell(2).setCellValue(o.getType());
			r.createCell(3).setCellValue(o.getDeskripsi());
			i++;
		}
		
		s.autoSizeColumn(0);
		s.autoSizeColumn(1);
		s.autoSizeColumn(2);
		s.autoSizeColumn(3);
	}

}
