package com.example.demo.validator.constraint;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.example.demo.validator.KategoriConstraintValidator;


@Target({ElementType.ANNOTATION_TYPE, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = KategoriConstraintValidator.class)
@Documented
public @interface Kategori {

	String message() default "Invalid.";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
