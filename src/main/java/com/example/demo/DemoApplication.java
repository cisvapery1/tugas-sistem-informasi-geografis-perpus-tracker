package com.example.demo;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.example.demo.entity.Kategori;
import com.example.demo.entity.Perpustakaan;
import com.example.demo.entity.Role;
import com.example.demo.entity.User;
import com.example.demo.repository.KategoriRepository;
import com.example.demo.repository.PerpustakaanRepository;
import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.UserRepository;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner{
	
	@Autowired
	private BCryptPasswordEncoder encode;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private KategoriRepository kategoriRepository;
	
	@Autowired
	private PerpustakaanRepository perpustakaanRepository;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Transactional
	@Override
	public void run(String... args) throws Exception {
		
		Role role = new Role();
		role.setName("ROLE_ADMIN");
		role.setDeskripsi("ADMIN");
		roleRepository.save(role);
		
		User user = new User();
		user.setEmail("peripurnama@mailnesia.com");
		user.setUsername("peripurnama");
		user.setPassword(encode.encode("admin"));
		user.addRole(role);
		userRepository.save(user);
		
		Kategori kategori = new Kategori();
		kategori.setName("Sekolah");
		kategori.setType("UMUM");
		kategori.setDeskripsi("UMUM");
		kategoriRepository.save(kategori);
		
		Perpustakaan perpus = new Perpustakaan();
		perpus.setAlamat("Perpustakaan ISBI Bandug");
		perpus.setDeskripsi("Umum");
		perpus.setJamBuka(new Date());
		perpus.setJamTutup(new Date());
		perpus.setKategori(kategori);
		perpus.setLatitude(107.62777870000002);
		perpus.setLongitude(-6.942634399999999);
		perpus.setName("Perpustakaan ISBI Bandug");
		perpus.setTelepon("088342342323");
		perpustakaanRepository.save(perpus);
	}

}
