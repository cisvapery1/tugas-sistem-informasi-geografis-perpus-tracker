package com.example.demo.dao;

import com.example.demo.dto.UserDto;
import com.example.demo.entity.User;

public interface UserDao extends CrudDao<User>, PaginationDao<UserDto>, ModelDto<UserDto>{
}