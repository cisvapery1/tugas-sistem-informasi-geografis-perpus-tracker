package com.example.demo.dao;

import com.example.demo.dto.KategoriDto;
import com.example.demo.entity.Kategori;

public interface KategoriDao extends CrudDao<Kategori>, PaginationDao<KategoriDto>, ModelDto<KategoriDto>{

}