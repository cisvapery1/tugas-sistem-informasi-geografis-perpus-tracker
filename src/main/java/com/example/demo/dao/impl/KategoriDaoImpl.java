package com.example.demo.dao.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.example.demo.dao.KategoriDao;
import com.example.demo.dto.KategoriDto;
import com.example.demo.entity.Kategori;
import com.example.demo.repository.KategoriRepository;

@Repository
public class KategoriDaoImpl implements KategoriDao{

	@Autowired
	private KategoriRepository kategoriRepository;
	
	@Override
	public Kategori save(Kategori t) {
		return kategoriRepository.save(t);
	}

	@Override
	public Kategori edit(Kategori t) {
		return kategoriRepository.save(t);
	}

	@Override
	public void delete(Long id) {
		Kategori obj = kategoriRepository.findById(id).get();
		kategoriRepository.delete(obj);
	}

	@Override
	public Kategori findById(Long id) {
		return kategoriRepository.findById(id).get();
	}

	@Override
	public Page<KategoriDto> pagination(Pageable pageable) {
		return kategoriRepository.pagination(pageable);
	}

	@Override
	public Page<KategoriDto> pagination(Pageable pageable, String search) {
		return kategoriRepository.pagination(pageable, search);
	}

	@Override
	public Optional<KategoriDto> findDtoById(Long id) {
		return kategoriRepository.dtoFindById(id);
	}

}
