package com.example.demo.dao.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.example.demo.dao.PerpustakaanDao;
import com.example.demo.dto.PerpustakaanDto;
import com.example.demo.entity.Perpustakaan;
import com.example.demo.repository.PerpustakaanRepository;

@Repository
public class PerpustakaanDaoImpl implements PerpustakaanDao{

	@Autowired
	private PerpustakaanRepository perpustakaanRepository;
	
	@Override
	public Perpustakaan save(Perpustakaan t) {
		return perpustakaanRepository.save(t);
	}

	@Override
	public Perpustakaan edit(Perpustakaan t) {
		return perpustakaanRepository.save(t);
	}

	@Override
	public void delete(Long id) {
		Perpustakaan obj = perpustakaanRepository.findById(id).get();
		perpustakaanRepository.delete(obj);
	}

	@Override
	public Perpustakaan findById(Long id) {
		return perpustakaanRepository.findById(id).get();
	}

	@Override
	public Page<PerpustakaanDto> pagination(Pageable pageable) {
		return perpustakaanRepository.pagination(pageable);
	}

	@Override
	public Page<PerpustakaanDto> pagination(Pageable pageable, String search) {
		return perpustakaanRepository.pagination(pageable, search);
	}

	@Override
	public Optional<PerpustakaanDto> findDtoById(Long id) {
		return perpustakaanRepository.dtoFindById(id);
	}

}
