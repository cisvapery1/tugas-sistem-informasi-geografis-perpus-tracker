package com.example.demo.dao.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.example.demo.dao.UserDao;
import com.example.demo.dto.UserDto;
import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;

@Repository
public class UserDaoImpl implements UserDao{

	@Autowired
	private UserRepository userRepository;
	
	@Override
	public User save(User t) {
		return userRepository.save(t);
	}

	@Override
	public User edit(User t) {
		return userRepository.save(t);
	}

	@Override
	public void delete(Long id) {
		User obj = userRepository.findById(id).get();
		userRepository.delete(obj);
	}

	@Override
	public User findById(Long id) {
		return userRepository.findById(id).get();
	}

	@Override
	public Page<UserDto> pagination(Pageable pageable) {
		return userRepository.pagination(pageable);
	}

	@Override
	public Page<UserDto> pagination(Pageable pageable, String search) {
		return userRepository.pagination(pageable, search);
	}

	@Override
	public Optional<UserDto> findDtoById(Long id) {
		return userRepository.dtoFindById(id);
	}

}
