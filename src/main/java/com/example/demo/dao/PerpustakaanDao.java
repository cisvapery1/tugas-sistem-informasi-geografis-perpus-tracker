package com.example.demo.dao;

import com.example.demo.dto.PerpustakaanDto;
import com.example.demo.entity.Perpustakaan;

public interface PerpustakaanDao extends CrudDao<Perpustakaan>, PaginationDao<PerpustakaanDto>, ModelDto<PerpustakaanDto>{

}