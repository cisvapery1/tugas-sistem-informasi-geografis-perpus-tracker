package com.example.demo.dao;

import java.util.Optional;

public interface ModelDto<T> {

	Optional<T> findDtoById(Long id);
	
}
