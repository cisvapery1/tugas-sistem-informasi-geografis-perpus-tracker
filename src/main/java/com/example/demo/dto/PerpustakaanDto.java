package com.example.demo.dto;

import java.util.Date;

import com.example.demo.entity.Perpustakaan;
import com.example.demo.util.LocalDateUtil;

public class PerpustakaanDto {

	private Long id;
	private double latitude;
	private double longitude;
	private String name;
	private Date jamBuka;
	private Date jamTutup;
	private String telepon;
	private String deskripsi;
	private String alamat;

	private KategoriDto kategori;

	private String createdBy;
	private Date createdDate;
	private String lastModifiedBy;
	private Date lastModifiedDate;
	private String formatCreatedDate;
	private String formatLastModifiedDate;

	private String formatJamBuka;
	private String formatJamTutup;
	private LocalDateUtil localDateUtil;

	public PerpustakaanDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PerpustakaanDto(Perpustakaan obj) {
		super();

		this.id = obj.getId();
		this.latitude = obj.getLatitude();
		this.longitude = obj.getLongitude();
		this.name = obj.getName();
		this.jamBuka = obj.getJamBuka();
		this.jamTutup = obj.getJamTutup();
		this.telepon = obj.getTelepon();
		this.alamat = obj.getAlamat();
		this.deskripsi = obj.getDeskripsi();

		localDateUtil = new LocalDateUtil();
		this.createdBy = obj.getCreatedBy();
		this.createdDate = obj.getCreatedDate();
		this.lastModifiedBy = obj.getLastModifiedBy();
		this.lastModifiedDate = obj.getLastModifiedDate();

		this.formatJamBuka = localDateUtil.formatTime(jamBuka);
		this.formatJamTutup = localDateUtil.formatTime(jamTutup);
		this.formatCreatedDate = localDateUtil.formatDate2(createdDate);
		this.formatLastModifiedDate = localDateUtil.formatDate2(lastModifiedDate);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getJamBuka() {
		return jamBuka;
	}

	public void setJamBuka(Date jamBuka) {
		this.jamBuka = jamBuka;
	}

	public Date getJamTutup() {
		return jamTutup;
	}

	public void setJamTutup(Date jamTutup) {
		this.jamTutup = jamTutup;
	}

	public String getTelepon() {
		return telepon;
	}

	public void setTelepon(String telepon) {
		this.telepon = telepon;
	}

	public String getDeskripsi() {
		return deskripsi;
	}

	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}

	public KategoriDto getKategori() {
		return kategori;
	}

	public void setKategori(KategoriDto kategori) {
		this.kategori = kategori;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getFormatCreatedDate() {
		return formatCreatedDate;
	}

	public void setFormatCreatedDate(String formatCreatedDate) {
		this.formatCreatedDate = formatCreatedDate;
	}

	public String getFormatLastModifiedDate() {
		return formatLastModifiedDate;
	}

	public void setFormatLastModifiedDate(String formatLastModifiedDate) {
		this.formatLastModifiedDate = formatLastModifiedDate;
	}

	public LocalDateUtil getLocalDateUtil() {
		return localDateUtil;
	}

	public void setLocalDateUtil(LocalDateUtil localDateUtil) {
		this.localDateUtil = localDateUtil;
	}

	public String getFormatJamBuka() {
		return formatJamBuka;
	}

	public void setFormatJamBuka(String formatJamBuka) {
		this.formatJamBuka = formatJamBuka;
	}

	public String getFormatJamTutup() {
		return formatJamTutup;
	}

	public void setFormatJamTutup(String formatJamTutup) {
		this.formatJamTutup = formatJamTutup;
	}

}
