package com.example.demo.dto;

import java.util.Date;

import com.example.demo.entity.User;
import com.example.demo.util.LocalDateUtil;

public class UserDto {

	private Long id;
	private String username;
	private String email;

	private String createdBy;
	private Date createdDate;
	private String lastModifiedBy;
	private Date lastModifiedDate;
	private String formatCreatedDate;
	private String formatLastModifiedDate;
	private LocalDateUtil localDateUtil;

	public UserDto() {
		super();

	}

	public UserDto(User obj) {

		this.id = obj.getId();
		this.username = obj.getUsername();
		this.email = obj.getEmail();

		localDateUtil = new LocalDateUtil();
		this.createdBy = obj.getCreatedBy();
		this.createdDate = obj.getCreatedDate();
		this.lastModifiedBy = obj.getLastModifiedBy();
		this.lastModifiedDate = obj.getLastModifiedDate();

		this.formatCreatedDate = localDateUtil.formatDate2(createdDate);
		this.formatLastModifiedDate = localDateUtil.formatDate2(lastModifiedDate);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getFormatCreatedDate() {
		return formatCreatedDate;
	}

	public void setFormatCreatedDate(String formatCreatedDate) {
		this.formatCreatedDate = formatCreatedDate;
	}

	public String getFormatLastModifiedDate() {
		return formatLastModifiedDate;
	}

	public void setFormatLastModifiedDate(String formatLastModifiedDate) {
		this.formatLastModifiedDate = formatLastModifiedDate;
	}

	public LocalDateUtil getLocalDateUtil() {
		return localDateUtil;
	}

	public void setLocalDateUtil(LocalDateUtil localDateUtil) {
		this.localDateUtil = localDateUtil;
	}

}
