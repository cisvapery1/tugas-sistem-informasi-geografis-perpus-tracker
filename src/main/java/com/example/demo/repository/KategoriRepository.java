package com.example.demo.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.dto.KategoriDto;
import com.example.demo.entity.Kategori;

public interface KategoriRepository extends JpaRepository<Kategori, Long> {

	@Query("select new com.example.demo.dto.KategoriDto(o) from Kategori o order by o.createdDate desc")
	Page<KategoriDto> pagination(Pageable pagebale);

	@Query("select new com.example.demo.dto.KategoriDto(o) from Kategori o "
			+ "where o.name like %?1% or o.type like %?1% " + "order by o.createdDate desc")
	Page<KategoriDto> pagination(Pageable pageabale, String search);

	@Query("select new com.example.demo.dto.KategoriDto(o) from Kategori o where o.id = ?1 ")
	Optional<KategoriDto> dtoFindById(Long id);
}