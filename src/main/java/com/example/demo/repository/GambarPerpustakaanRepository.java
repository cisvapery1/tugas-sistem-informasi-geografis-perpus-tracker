package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.GambarPerpustakaan;

public interface GambarPerpustakaanRepository extends JpaRepository<GambarPerpustakaan, Long>{

}
