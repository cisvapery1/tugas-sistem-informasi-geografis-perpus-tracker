package com.example.demo.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.dto.UserDto;
import com.example.demo.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {

	Optional<User> findByUsername(String username);

	Optional<User> findByEmail(String email);
	
	Optional<User> findByUsernameOrEmail(String username, String email);
	
	@Query("select new com.example.demo.dto.UserDto(o) from User o order by o.createdDate desc")
	Page<UserDto> pagination(Pageable pagebale);
	
	@Query("select new com.example.demo.dto.UserDto(o) from User o "
			+ "where o.username like %?1% or o.email like %?1% "
			+ "order by o.createdDate desc")
	Page<UserDto> pagination(Pageable pageabale, String search);
	
	@Query("select new com.example.demo.dto.UserDto(o) from User o where o.id = ?1 ")
	Optional<UserDto> dtoFindById(Long id);
	
}
