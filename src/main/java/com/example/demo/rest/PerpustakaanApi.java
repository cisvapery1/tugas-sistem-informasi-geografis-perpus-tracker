package com.example.demo.rest;

import java.security.Principal;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.dto.PerpustakaanDto;
import com.example.demo.entity.Kategori;
import com.example.demo.service.PerpustakaanService;
import com.example.demo.util.ModelDataTable;

@RestController
@RequestMapping("/rest/perpustakaan")
public class PerpustakaanApi {

	@Autowired
	private PerpustakaanService perpustakaanService;
	
	@GetMapping
	public ModelDataTable<PerpustakaanDto> pagination(Optional<Integer> pageSize, Optional<Integer> page,
			Optional<String> search, Optional<String> sortBy, Optional<String> sortDirection) {
		return perpustakaanService.pagination(pageSize, page, search, sortBy, sortDirection);
	}
	
	@GetMapping("/report")
	public ModelAndView report(Optional<Integer> pageSize, Optional<Integer> page, Optional<String> search,
			Optional<String> sortBy, Optional<String> sortDirection, Model model, HttpServletRequest req,
			HttpServletResponse res, Principal principal, HttpSession session) {
		
		return perpustakaanService.report(pageSize, page, search, sortBy, sortDirection, model, req, res, principal, session);
	}
	
	@GetMapping("/show/{id}")
	public ResponseEntity<?> dtoFindById(@PathVariable(value = "id") Long id) {
		return new ResponseEntity<PerpustakaanDto>(perpustakaanService.dtoFindById(id), HttpStatus.OK);
	}
	
	@GetMapping("/delete/{id}")
	public ResponseEntity<?> delete(@PathVariable(value = "id") Long id) {
		perpustakaanService.delete(id);
		return new ResponseEntity<String>("success", HttpStatus.OK);
	}
}
