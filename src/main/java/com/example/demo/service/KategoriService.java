package com.example.demo.service;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.dao.KategoriDao;
import com.example.demo.dto.KategoriDto;
import com.example.demo.entity.Kategori;
import com.example.demo.report.excel.ExcelKategoriReport;
import com.example.demo.report.pdf.PdfKategoriReport;
import com.example.demo.util.ModelDataTable;
import com.example.demo.util.PagerModel;

@Service
public class KategoriService {

	@Autowired
	private KategoriDao kategoriDao;
	
	private static final int BUTTONS_TO_SHOW = 5;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 3;
	private static final int[] PAGE_SIZES = { 3, 5, 10, 25, 50, 75, 100 };

	public Kategori save(Kategori kategori) {
		return kategoriDao.save(kategori);
	}

	public Kategori edit(Kategori kategori) {
		return kategoriDao.save(kategori);
	}

	public void delete(Long id) {
		kategoriDao.delete(id);
	}

	public Kategori findById(Long id) {
		return kategoriDao.findById(id);
	}
	
	public Optional<KategoriDto> dtoFindById(Long id) {
		return kategoriDao.findDtoById(id);
	}

	public ModelDataTable<KategoriDto> pagination(Optional<Integer> pageSize, Optional<Integer> page,
			Optional<String> search, Optional<String> sortBy, Optional<String> sortDirection) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		String evalSearch = search.orElse("");
		String evalSortBy = sortBy.orElse("ASC");
		String evalsortDirection = sortDirection.orElse("id");
		Page<KategoriDto> pages = null;

		Direction direction;
		if (evalSortBy.equals("ASC")) {
			direction = Sort.Direction.ASC;
		} else {
			direction = Sort.Direction.DESC;
		}

		if (evalSearch.equals("")) {
			pages = kategoriDao.pagination(PageRequest.of(evalPage, evalPageSize, direction, evalsortDirection));
		} else {
			pages = kategoriDao.pagination(PageRequest.of(evalPage, evalPageSize, direction, evalsortDirection),
					evalSearch);
		}

		PagerModel pager = new PagerModel(pages.getTotalPages(), pages.getNumber(), BUTTONS_TO_SHOW);
		ModelDataTable<KategoriDto> model = new ModelDataTable<>();
		model.setData(pages);
		model.setEvalPage(evalPage);
		model.setPagerModel(pager);
		model.setPageSize(PAGE_SIZES);
		model.setSelectedPageSize(evalPageSize);
//		model.setTotalRow(bagianDaoImpl.findAll().size());
		model.setResultCount(pages.getSize());
//		model.setAuthName(auth.getAuthentication().getName());

		return model;
	}

	public ModelAndView report(Optional<Integer> pageSize, Optional<Integer> page, Optional<String> search,
			Optional<String> sortBy, Optional<String> sortDirection, Model model, HttpServletRequest req,
			HttpServletResponse res, Principal principal, HttpSession session) {

		String type = req.getParameter("type");

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		String evalSearch = search.orElse("");
		String evalSortBy = sortBy.orElse("ASC");
		String evalsortDirection = sortDirection.orElse("id");

		Page<KategoriDto> pages = null;

		Direction direction;
		if (evalSortBy.equals("ASC")) {
			direction = Sort.Direction.ASC;
		} else {
			direction = Sort.Direction.DESC;
		}

		if (evalSearch.equals("")) {
			pages = kategoriDao.pagination(PageRequest.of(evalPage, evalPageSize, direction, evalsortDirection));
		} else {
			pages = kategoriDao.pagination(
					PageRequest.of(evalPage, evalPageSize, direction, evalsortDirection), evalSearch);
		}

		List<KategoriDto> lists = pages.getContent();

		int number = pages.getNumber();
		int size = pages.getSize();

		Map<String, Object> mapModel = new HashMap<>();
		mapModel.put("reports", lists);
		mapModel.put("number", number);
		mapModel.put("size", size);

		ExcelKategoriReport excel = new ExcelKategoriReport();
		PdfKategoriReport pdf = new PdfKategoriReport();
		if (type.equals("xls")) {
			return new ModelAndView(excel, mapModel);
		} else if (type.equals("pdf")) {
			return new ModelAndView(pdf, mapModel);
		}
		return new ModelAndView(new ExcelKategoriReport(), mapModel);
	}
}
