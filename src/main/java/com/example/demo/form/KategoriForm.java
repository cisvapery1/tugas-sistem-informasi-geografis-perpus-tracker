package com.example.demo.form;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.example.demo.validator.constraint.Kategori;

@Kategori
public class KategoriForm {

	private Long id;
	
	@NotNull(message = "Nama harus di isi")
	@Size(min = 3, max = 75, message = "Nama minimal 3 dan maksimal 75 huruf")
	private String name;
	
	@NotNull(message = "Type harus di isi")
	private String type;
	
	@NotNull(message = "Deskripsi harus di isi")
	@Size(min = 3, max = 75, message = "Deskripsi minimal 3 dan maksimal 150 huruf")
	private String deskripsi;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDeskripsi() {
		return deskripsi;
	}

	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}

}