package com.example.demo.form;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PerpustakaanForm {

	private Long id;

	private double latitude;
	private double longitude;

	@NotNull(message = "Nama harus di isi")
	private String name;

	@NotNull(message = "Jam buka harus di isi")
	private String jamBuka;

	@NotNull(message = "Jam tutup harus di isi")
	private String jamTutup;

	@NotNull(message = "Telepon harus di isi")
	private String telepon;

	@NotNull(message = "Alamat harus di isi")
	private String alamat;

	@NotNull(message = "Deskripsi harus di isi")
	private String deskripsi;

	@NotNull(message = "Kategori harus di isi")
	private Long kategoriId;

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getJamBuka() {
		return jamBuka;
	}

	public void setJamBuka(String jamBuka) {
		this.jamBuka = jamBuka;
	}

	public String getJamTutup() {
		return jamTutup;
	}

	public void setJamTutup(String jamTutup) {
		this.jamTutup = jamTutup;
	}

	public String getTelepon() {
		return telepon;
	}

	public void setTelepon(String telepon) {
		this.telepon = telepon;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public String getDeskripsi() {
		return deskripsi;
	}

	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}

	public Long getKategoriId() {
		return kategoriId;
	}

	public void setKategoriId(Long kategoriId) {
		this.kategoriId = kategoriId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "PerpustakaanForm [latitude=" + latitude + ", longitude=" + longitude + ", name=" + name + ", jamBuka="
				+ jamBuka + ", jamTutup=" + jamTutup + ", telepon=" + telepon + ", alamat=" + alamat + ", deskripsi="
				+ deskripsi + ", kategoriId=" + kategoriId + "]";
	}

}
